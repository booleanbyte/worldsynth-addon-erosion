/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package com.booleanbyte.worldsynth.addon;

import java.util.ArrayList;
import java.util.List;

import com.booleanbyte.worldsynth.addon.erosion.ErosionModuleRegister;

import net.worldsynth.WorldSynthDirectoryConfig;
import net.worldsynth.addon.IAddon;
import net.worldsynth.biome.BiomeProfile;
import net.worldsynth.customobject.CustomObjectFormat;
import net.worldsynth.material.MaterialProfile;
import net.worldsynth.module.AbstractModuleRegister;
import net.worldsynth.patcher.WorldSynthPatcher;

public class AddonErosion implements IAddon {
	
	public static void main(String[] args) {
		WorldSynthPatcher.startPatcher(args, new AddonErosion());
	}
	
	@Override
	public void initAddon(WorldSynthDirectoryConfig directoryConfig) {}

	@Override
	public List<AbstractModuleRegister> getAddonModuleRegisters() {
		ArrayList<AbstractModuleRegister> moduleRegisters = new ArrayList<AbstractModuleRegister>();
		moduleRegisters.add(new ErosionModuleRegister());
		return moduleRegisters;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public List<MaterialProfile> getAddonMaterialProfiles() {
		ArrayList<MaterialProfile> materialProfiles = new ArrayList<MaterialProfile>();
		return materialProfiles;
	}
	
	@SuppressWarnings("rawtypes")
	@Override
	public List<BiomeProfile> getAddonBiomeProfiles() {
		ArrayList<BiomeProfile> materialProfiles = new ArrayList<BiomeProfile>();
		return materialProfiles;
	}

	@Override
	public List<CustomObjectFormat> getAddonCustomObjectFormats() {
		ArrayList<CustomObjectFormat> objectFormats = new ArrayList<CustomObjectFormat>();
		return objectFormats;
	}
}
