/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package com.booleanbyte.worldsynth.addon.erosion.module.heightmap;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import com.booleanbyte.worldsynth.addon.erosion.implementation.ParticleErosionUnflat;

import net.worldsynth.datatype.AbstractDatatype;
import net.worldsynth.datatype.DatatypeHeightmap;
import net.worldsynth.module.AbstractModule;
import net.worldsynth.module.IModuleCategory;
import net.worldsynth.module.ModuleCategory;
import net.worldsynth.module.ModuleInput;
import net.worldsynth.module.ModuleInputRequest;
import net.worldsynth.module.ModuleOutput;
import net.worldsynth.module.ModuleOutputRequest;
import net.worldsynth.parameter.AbstractParameter;
import net.worldsynth.parameter.FloatParameter;
import net.worldsynth.parameter.IntegerParameter;
import net.worldsynth.parameter.LongParameter;

public class ModuleHeightmapParticleErosion extends AbstractModule {
	
	private LongParameter seed = new LongParameter("seed", "Seed", null, 0, Long.MIN_VALUE, Long.MAX_VALUE);
	private FloatParameter duration = new FloatParameter("duration", "Duration", null, 1, 0, Float.POSITIVE_INFINITY, 0, 10);
	
	private IntegerParameter erosionRadius = new IntegerParameter("erosionradius", "Erosion radius", null, 3, 2, 10);
	private FloatParameter inertia = new FloatParameter("inertia", "Inertia", "At zero, water will instantly change direction to flow downhill. At 1, water will never change direction.", 0.05f, 0.0f, 1.0f);
	private FloatParameter sedimentCapacityFactor = new FloatParameter("sedimentcapacity", "Sediment capacity factor", "Multiplier for how much sediment a droplet can carry", 4.0f, 0.0f, 10.0f);
	private FloatParameter minSedimentCapacity = new FloatParameter("minsedimentcapacity", "Min sediment capacity", "Used to prevent carry capacity getting too close to zero on flatter terrain", 0.01f, 0.0f, 1.0f, 0.0f, 0.1f);

	private FloatParameter erodeSpeed = new FloatParameter("erodespeed", "Erode speed", null, 0.3f, 0.0f, 1.0f);
	private FloatParameter depositSpeed = new FloatParameter("depositspeed", "Deposit speed", null, 0.03f, 0.0f, 1.0f);
	private FloatParameter evaporateSpeed = new FloatParameter("evaporatespeed", "Evaporate speed", null, 0.01f, 0.0f, 1.0f);
	
	private FloatParameter gravity = new FloatParameter("gravity", "Gravity", null, 4.0f, 0.0f, Float.POSITIVE_INFINITY, 0.0f, 10.0f);
	private IntegerParameter maxDropletLifetime = new IntegerParameter("dropletlifetime", "Droplet lifetime", null, 30, 1, Integer.MAX_VALUE, 1, 100);

	private FloatParameter initialWaterVolume = new FloatParameter("watervolume", "Water volume", null, 1.0f, 0.0f, 10.0f);
	private FloatParameter initialSpeed = new FloatParameter("initialspeed", "Initial speed", null, 1.0f, 0.0f, 10.0f);

	
	@Override
	public AbstractParameter<?>[] registerParameters() {
		
		seed.setValue(new Random().nextLong());
		
		AbstractParameter<?>[] p = {
				duration,
				erosionRadius,
				inertia,
				sedimentCapacityFactor,
				minSedimentCapacity,
				erodeSpeed,
				depositSpeed,
				evaporateSpeed,
				gravity,
				maxDropletLifetime,
				initialWaterVolume,
				initialSpeed,
				seed
				};
		return p;
	}
	
	@Override
	public Map<String, ModuleInputRequest> getInputRequests(ModuleOutputRequest outputRequest) {
		HashMap<String, ModuleInputRequest> inputRequests = new HashMap<String, ModuleInputRequest>();
		
		inputRequests.put("input", new ModuleInputRequest(getInput("Primary input"), outputRequest.data));
		
		return inputRequests;
	}

	@Override
	public AbstractDatatype buildModule(Map<String, AbstractDatatype> inputs, ModuleOutputRequest request) {
		DatatypeHeightmap requestData = (DatatypeHeightmap) request.data;
		
		int mpw = requestData.mapPointsWidth;
		int mpl = requestData.mapPointsLength;
		
		//----------READ INPUTS----------//
		
		//Read in primary input
		if(inputs.get("input") == null) {
			//If the main input (index 0) is null, there is no input and then just return null
			return null;
		}
		float[][] inputMap = ((DatatypeHeightmap) inputs.get("input")).getHeightmap();
		
		int particleIterations = (int) (duration.getValue() * mpw * mpl);
		erodeUnflat(inputMap, particleIterations);
		
		//----------BUILD----------//
		
		requestData.setHeightmap(inputMap);
		
		return requestData;
	}
	
	private void erodeUnflat(float[][] map, int particleIterations) {
		ParticleErosionUnflat erosion = new ParticleErosionUnflat(
				seed.getValue(),
				erosionRadius.getValue(),
				inertia.getValue(),
				sedimentCapacityFactor.getValue(),
				minSedimentCapacity.getValue(),
				erodeSpeed.getValue(),
				depositSpeed.getValue(),
				evaporateSpeed.getValue(),
				gravity.getValue(),
				maxDropletLifetime.getValue(),
				initialWaterVolume.getValue(),
				initialSpeed.getValue());
		
		erosion.erode(map, map.length, map[0].length, particleIterations);
	}

	@Override
	public String getModuleName() {
		return "Particle erosion";
	}

	@Override
	public IModuleCategory getModuleCategory() {
		return ModuleCategory.MODIFIER;
	}

	@Override
	public ModuleInput[] registerInputs() {
		ModuleInput[] i = {
				new ModuleInput(new DatatypeHeightmap(), "Primary input"),
				};
		return i;
	}

	@Override
	public ModuleOutput[] registerOutputs() {
		ModuleOutput[] o = {
				new ModuleOutput(new DatatypeHeightmap(), "Primary output")
				};
		return o;
	}

	@Override
	public boolean isBypassable() {
		return true;
	}

}
