/*
 * MIT License
 * 
 * Copyright (c) 2019 Sebastian Lague
 * Copyright (c) 2020 booleanbyte
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 * 
 * ------------------------------------------------------------------------------
 * 
 * Ported to java with modifications by booleanbyte
 * The original C# code by Sebastian Lague can be found at:
 * https://github.com/SebLague/Hydraulic-Erosion
 */
package com.booleanbyte.worldsynth.addon.erosion.implementation;

import java.util.Random;

public class ParticleErosionUnflatAdvanced {

	private long seed; // [Range (2, 8)]

	private int erosionRadius = 3; // [Range (0, 1)]

	private float inertia = .05f; // At zero, water will instantly change direction to flow downhill. At 1, water will never change direction.
	private float sedimentCapacityFactor = 4; // Multiplier for how much sediment a droplet can carry
	private float minSedimentCapacity = .01f; // Used to prevent carry capacity getting too close to zero on flatter terrain [Range (0, 1)]

	private float erodeSpeed = .3f; // [Range (0, 1)]
	private float depositSpeed = .3f; // [Range (0, 1)]
	private float evaporateSpeed = .01f;
	private float gravity = 4;
	private int maxDropletLifetime = 30;

	private float initialWaterVolume = 1;
	private float initialSpeed = 1;

	// Indices and weights of erosion brush precomputed for every node
	private int[][][] erosionBrushIndices;
	private float[][][] erosionBrushWeights;
	private Random prng;

	// Initialization creates a System.Random object and precomputes indices and
	// weights of erosion brush
	public ParticleErosionUnflatAdvanced(
			long seed,
			int erosionRadius,
			float inertia,
			float sedimentCapacityFactor,
			float minSedimentCapacity,
			float erodeSpeed,
			float depositSpeed,
			float evaporateSpeed,
			float gravity,
			int maxDropletLifetime,
			float initialWaterVolume,
			float initialSpeed) {
		
		this.seed = seed;
		this.erosionRadius = erosionRadius;
		this.inertia = inertia;
		this.sedimentCapacityFactor = sedimentCapacityFactor;
		this.minSedimentCapacity = minSedimentCapacity;
		this.erodeSpeed = erodeSpeed;
		this.depositSpeed = depositSpeed;
		this.evaporateSpeed = evaporateSpeed;
		this.gravity = gravity;
		this.maxDropletLifetime = maxDropletLifetime;
		this.initialWaterVolume = initialWaterVolume;
		this.initialSpeed = initialSpeed;
	}

	public void erode(float[][] map, float[][] flowmap, float[][] wearmap, float[][] depositionmap, int mapWidth, int mapLength, int numIterations) {
		
		prng = new Random(seed);
		initializeBrushIndices(mapWidth, mapLength, erosionRadius);
		
		for(int iteration = 0; iteration < numIterations; iteration++) {
			// Create water droplet at random point on map
			float posX = prng.nextFloat() * (mapWidth - 1);
			float posY = prng.nextFloat() * (mapLength - 1);
			float dirX = 0;
			float dirY = 0;
			float speed = initialSpeed;
			float water = initialWaterVolume;
			float sediment = 0;

			for(int lifetime = 0; lifetime < maxDropletLifetime; lifetime++) {
				int nodeX = (int) posX;
				int nodeY = (int) posY;
				// Calculate droplet's offset inside the cell (0,0) = at NW node, (1,1) = at SE
				// node
				float cellOffsetX = posX - nodeX;
				float cellOffsetY = posY - nodeY;

				// Calculate droplet's height and direction of flow with bilinear interpolation
				// of surrounding heights
				HeightAndGradient heightAndGradient = calculateHeightAndGradient(map, posX, posY);

				// Update the droplet's direction and position (move position 1 unit regardless
				// of speed)
				dirX = (dirX * inertia - heightAndGradient.gradientX * (1 - inertia));
				dirY = (dirY * inertia - heightAndGradient.gradientY * (1 - inertia));
				// Normalize direction
				float len = (float) Math.sqrt(dirX * dirX + dirY * dirY);
				if(len != 0) {
					dirX /= len;
					dirY /= len;
				}
				posX += dirX;
				posY += dirY;
				
				// Flowmap
				flowmap[nodeX][nodeY] += 1.0f * (1 - cellOffsetX) * (1 - cellOffsetY);
				flowmap[nodeX + 1][nodeY] += 1.0f * cellOffsetX * (1 - cellOffsetY);
				flowmap[nodeX][nodeY + 1] += 1.0f * (1 - cellOffsetX) * cellOffsetY;
				flowmap[nodeX + 1][nodeY + 1] += 1.0f * cellOffsetX * cellOffsetY;

				// Stop simulating droplet if it's not moving or has flowed over edge of map
				if((dirX == 0 && dirY == 0) || posX < 0 || posX >= mapWidth - 1 || posY < 0 || posY >= mapLength - 1) {
					break;
				}

				// Find the droplet's new height and calculate the deltaHeight
				float newHeight = calculateHeightAndGradient(map, posX, posY).height;
				float deltaHeight = newHeight - heightAndGradient.height;

				// Calculate the droplet's sediment capacity (higher when moving fast down a
				// slope and contains lots of water)
				float sedimentCapacity = Math.max(-deltaHeight * speed * water * sedimentCapacityFactor, minSedimentCapacity);

				// If carrying more sediment than capacity, or if flowing uphill:
				if(sediment > sedimentCapacity || deltaHeight > 0) {
					// If moving uphill (deltaHeight > 0) try fill up to the current height,
					// otherwise deposit a fraction of the excess sediment
					float amountToDeposit = (deltaHeight > 0) ? Math.min(deltaHeight, sediment) : (sediment - sedimentCapacity) * depositSpeed;
					sediment -= amountToDeposit;

					// Add the sediment to the four nodes of the current cell using bilinear
					// interpolation
					// Deposition is not distributed over a radius (like erosion) so that it can
					// fill small pits
					map[nodeX][nodeY] += amountToDeposit * (1 - cellOffsetX) * (1 - cellOffsetY);
					map[nodeX + 1][nodeY] += amountToDeposit * cellOffsetX * (1 - cellOffsetY);
					map[nodeX][nodeY + 1] += amountToDeposit * (1 - cellOffsetX) * cellOffsetY;
					map[nodeX + 1][nodeY + 1] += amountToDeposit * cellOffsetX * cellOffsetY;
					
					// Depositionmap
					depositionmap[nodeX][nodeY] += amountToDeposit * (1 - cellOffsetX) * (1 - cellOffsetY);
					depositionmap[nodeX + 1][nodeY] += amountToDeposit * cellOffsetX * (1 - cellOffsetY);
					depositionmap[nodeX][nodeY + 1] += amountToDeposit * (1 - cellOffsetX) * cellOffsetY;
					depositionmap[nodeX + 1][nodeY + 1] += amountToDeposit * cellOffsetX * cellOffsetY;
				}
				else {
					// Erode a fraction of the droplet's current carry capacity.
					// Clamp the erosion to the change in height so that it doesn't dig a hole in
					// the terrain behind the droplet
					float amountToErode = Math.min((sedimentCapacity - sediment) * erodeSpeed, -deltaHeight);

					// Use erosion brush to erode from all nodes inside the droplet's erosion radius
					for(int brushPointIndex = 0; brushPointIndex < erosionBrushIndices[nodeX][nodeY].length / 2; brushPointIndex++) {
						int nodeIndexX = erosionBrushIndices[nodeX][nodeY][brushPointIndex * 2];
						int nodeIndexY = erosionBrushIndices[nodeX][nodeY][brushPointIndex * 2 + 1];
						float weighedErodeAmount = amountToErode * erosionBrushWeights[nodeX][nodeY][brushPointIndex];
						float deltaSediment = (map[nodeIndexX][nodeIndexY] < weighedErodeAmount) ? map[nodeIndexX][nodeIndexY] : weighedErodeAmount;
						
						map[nodeIndexX][nodeIndexY] -= deltaSediment;
						
						// Wearmap
						wearmap[nodeIndexX][nodeIndexY] += deltaSediment;
						
						sediment += deltaSediment;
					}
				}

				// Update droplet's speed and water content
				//
				// The original implementation has problems with getting NaN speeds because 
				//    speed * speed + deltaHeight * gravity
				// can give negative numbers in the root.
				// Speed is the magnitude of velocity and does not have a directional value
				// and should not be negative, therefore we do the equivalent of taking the
				// absolute value before the root, and inverting the direction values when
				// the speed crosses zero.
				float s = speed * speed + deltaHeight * gravity;
				if(s < 0) {
					s *= -1;
					dirX *= -1;
					dirY *= -1;
				}
				speed = (float) Math.sqrt(s);
				water *= (1 - evaporateSpeed);
			}
		}
	}

	private HeightAndGradient calculateHeightAndGradient (float[][] nodes, float posX, float posY) {
        int coordX = (int) posX;
        int coordY = (int) posY;

        // Calculate droplet's offset inside the cell (0,0) = at NW node, (1,1) = at SE node
        float x = posX - coordX;
        float y = posY - coordY;

        // Calculate heights of the four nodes of the droplet's cell
        float heightNW = nodes[coordX][coordY];
        float heightNE = nodes[coordX + 1][coordY];
        float heightSW = nodes[coordX][coordY + 1];
        float heightSE = nodes[coordX + 1][coordY + 1];

        // Calculate droplet's direction of flow with bilinear interpolation of height difference along the edges
        float gradientX = (heightNE - heightNW) * (1 - y) + (heightSE - heightSW) * y;
        float gradientY = (heightSW - heightNW) * (1 - x) + (heightSE - heightNE) * x;

        // Calculate height with bilinear interpolation of the heights of the nodes of the cell
        float height = heightNW * (1 - x) * (1 - y) + heightNE * x * (1 - y) + heightSW * (1 - x) * y + heightSE * x * y;

        return new HeightAndGradient (height, gradientX, gradientY);
    }

	private void initializeBrushIndices(int mapWidth, int mapLength, int radius) {
		erosionBrushIndices = new int[mapWidth][mapLength][];
		erosionBrushWeights = new float[mapWidth][mapLength][];

		int[] xOffsets = new int[radius * radius * 4];
		int[] yOffsets = new int[radius * radius * 4];
		float[] weights = new float[radius * radius * 4];
		float weightSum = 0.0f;
		int addIndex = 0;

		for(int u = 0; u < mapWidth; u++) {
			for(int v = 0; v < mapLength; v++) {
				int centreX = u;
				int centreY = v;
				
				if(centreY <= radius || centreY >= mapLength - radius || centreX <= radius + 1 || centreX >= mapWidth - radius) {
					weightSum = 0.0f;
					addIndex = 0;
					for(int y = -radius; y <= radius; y++) {
						for(int x = -radius; x <= radius; x++) {
							float sqrDst = x * x + y * y;
							if(sqrDst < radius * radius) {
								int coordX = centreX + x;
								int coordY = centreY + y;

								if(coordX >= 0 && coordX < mapWidth && coordY >= 0 && coordY < mapLength) {
									float weight = 1.0f - (float) Math.sqrt(sqrDst) / (float) radius;
									weightSum += weight;
									weights[addIndex] = weight;
									xOffsets[addIndex] = x;
									yOffsets[addIndex] = y;
									addIndex++;
								}
							}
						}
					}
				}
				
				erosionBrushIndices[u][v] = new int[addIndex * 2];
				erosionBrushWeights[u][v] = new float[addIndex];

				for(int j = 0; j < addIndex; j++) {
					erosionBrushIndices[u][v][j*2] = xOffsets[j] + centreX;
					erosionBrushIndices[u][v][j*2 + 1] = yOffsets[j] + centreY;
					erosionBrushWeights[u][v][j] = weights[j] / weightSum;
				}
			}
		}
	}

	private class HeightAndGradient {
		public float height;
		public float gradientX;
		public float gradientY;
		
		public HeightAndGradient(float height, float gradientX, float gradientY) {
			this.height = height;
			this.gradientX = gradientX;
			this.gradientY = gradientY;
		}
	}
}
