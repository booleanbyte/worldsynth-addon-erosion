/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package com.booleanbyte.worldsynth.addon.erosion;

import com.booleanbyte.worldsynth.addon.erosion.module.heightmap.ModuleHeightmapParticleErosion;
import com.booleanbyte.worldsynth.addon.erosion.module.heightmap.ModuleHeightmapParticleErosionAdvanced;

import net.worldsynth.module.AbstractModuleRegister;
import net.worldsynth.module.ClassNotModuleExeption;

public class ErosionModuleRegister extends AbstractModuleRegister {
	
	public ErosionModuleRegister() {
		super();
		
		try {
			registerModule(ModuleHeightmapParticleErosion.class, "\\Erosion");
			registerModule(ModuleHeightmapParticleErosionAdvanced.class, "\\Erosion");
		} catch (ClassNotModuleExeption e) {
			throw new RuntimeException(e);
		}
	}
}
